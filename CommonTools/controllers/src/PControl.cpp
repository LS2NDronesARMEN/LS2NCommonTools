#include "controllers/PControl.h"

namespace Controllers
{
  /** Constructor
  *
  **/
  PControl::PControl(unsigned int _size)
  : ControlLawBase(_size)
  {
    if(size > 0)
    {
      kp = Eigen::MatrixXd::Identity(size,size);
      state = Eigen::VectorXd(size);
      ref = Eigen::VectorXd(size);
      out = Eigen::VectorXd(size);
    }
    else
    {
      ROS_ERROR("must give a size to the control block");
    }
    return;
  }


  /** Destructor
  *
  **/
  PControl::~PControl()
  {
    return;
  }


  /** Calculate
  *
  **/
  void PControl::Calculate()
  {
    out = kp * (ref-state);
    first_iter = false;
    return;
  }



  /** \overload void PControl::SetState(double _state)
  *
  **/
  void PControl::SetState(double _state)
  {
    convert2VXd(state,_state);
    return;
  }
  /** \overload void PControl::SetState(Eigen::MatrixXd _state);
  *
  **/
  void PControl::SetState(Eigen::MatrixXd _state)
  {
    convert2VXd(state,_state);
    return;
  }



  /** void PControl::SetRef(double _state)
  *
  **/
  void PControl::SetRef(double _ref)
  {
    convert2VXd(ref,_ref);
    return;
  }
  /** \overload void PControl::SetRef(Eigen::MatrixXd _state);
  *
  **/
  void PControl::SetRef(Eigen::MatrixXd _ref)
  {
    convert2VXd(ref,_ref);
    return;
  }



  /** void PControl::SetGains(double _state)
  *
  **/
  void PControl::SetGains(double _kp)
  {
    kp = _kp*Eigen::MatrixXd::Identity(size,size);
    return;
  }
  /** \overload void PControl::SetGains(Eigen::MatrixXd _state);
  *
  **/
  void PControl::SetGains(Eigen::MatrixXd _kp)
  {
    if(_kp.rows() == size && _kp.cols() ==size)
    {
      kp = _kp.diagonal().asDiagonal();
    }
    else if (_kp.rows() == size)
    {
      kp = _kp.asDiagonal();
    }
    else
    {
      ROS_ERROR("Cannot Set the gains to that size");
    }
    return;
  }


}
