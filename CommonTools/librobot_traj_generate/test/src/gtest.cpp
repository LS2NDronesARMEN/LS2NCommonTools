#include <gtest/gtest.h>
#include "librobot_traj_generate/librobot_traj_generate.hpp"
#include <Eigen/Dense>



TEST(LibTraGenerateTest, testTrajectoryPoint1)
{
    Eigen::VectorXd q0(6);
    q0<<0,0,0,0,0,0;
    Eigen::VectorXd qf(6);
    qf<<1,0,0,0,0,0;    
    double tf = 1;

    LibTrajGenerate trajectory_generator(q0, qf, tf);

    Eigen::VectorXd q;
    Eigen::VectorXd dq;
    Eigen::VectorXd ddq;

    double time_step = 0;
    trajectory_generator.doFifthDegreeTrajectory(time_step);
    trajectory_generator.getTrajectory(q, dq, ddq);

    // check initial condition
    std::cout<<"[----------] Begin: one-point trajectory initial pose test"<<std::endl;
    ASSERT_EQ(q, q0);
    ASSERT_EQ(dq, Eigen::MatrixXd::Zero(6,1));
    ASSERT_EQ(ddq,Eigen::MatrixXd::Zero(6,1));
    std::cout<<"[----------] Finish: one-point trajectory initial pose test"<<std::endl;

    time_step = tf;
    trajectory_generator.doFifthDegreeTrajectory(time_step);
    trajectory_generator.getTrajectory(q, dq, ddq);


    // check final condition
    std::cout<<"[----------] Bgein: one-point trajectory final pose test "<<std::endl;
    ASSERT_EQ(q, qf);
    ASSERT_EQ(dq, Eigen::MatrixXd::Zero(6,1));
    ASSERT_EQ(ddq,Eigen::MatrixXd::Zero(6,1));
    std::cout<<"[----------] Finish: one-point trajectory final pose test "<<std::endl;


    // check one point
    std::cout<<"[----------] Begin: one-point trajectory one waypoint test"<<std::endl;
    time_step = 0.6;
    trajectory_generator.doFifthDegreeTrajectory(time_step);
   
    trajectory_generator.getTrajectory(q, dq, ddq);
   
    Eigen::VectorXd q_des(6);
    q_des<<0.68255999999,0,0,0,0,0;
  
    Eigen::VectorXd dq_des(6);
    dq_des<<1.72800000000,0,0,0,0,0;
    Eigen::VectorXd ddq_des(6);
    ddq_des<<-2.8800000,0,0,0,0,0;
 
    ASSERT_NEAR(q(0), q_des(0), 1*pow(10,-4));
    ASSERT_NEAR(dq(0), dq_des(0), 1*pow(10,-4));
    ASSERT_NEAR(ddq(0), ddq_des(0), 1*pow(10,-4));        
    std::cout<<"[----------] Finish: one-point trajectory one waypoint test"<<std::endl;

    EXPECT_TRUE(true);
}

TEST(LibTraGenerateTest, testTrajectoryPoint2)
{
    Eigen::VectorXd q0(6);
    q0<<0.1,-10, 0.002, 0.55, 1, 2;
    Eigen::VectorXd qf(6);
    qf<<20,10, -2, 2, 0.6,-10;    
    double tf = 10;

    LibTrajGenerate trajectory_generator(q0, qf, tf);

    Eigen::VectorXd q;
    Eigen::VectorXd dq;
    Eigen::VectorXd ddq;

    double time_step = 0;
    trajectory_generator.doFifthDegreeTrajectory(time_step);
    trajectory_generator.getTrajectory(q, dq, ddq);

    // check initial condition
    std::cout<<"[----------] Begin: multi-point trajectory intial pose "<<std::endl;
    ASSERT_EQ(q(0), q0(0));
    ASSERT_EQ(q(1), q0(1));
    ASSERT_EQ(q(2), q0(2));
    ASSERT_EQ(q(3), q0(3));
    ASSERT_EQ(q(4), q0(4));
    ASSERT_EQ(q(5), q0(5));
    ASSERT_EQ(dq(0), 0);
    ASSERT_EQ(dq(1), 0);
    ASSERT_EQ(dq(2), 0);
    ASSERT_EQ(dq(3), 0);
    ASSERT_EQ(dq(4), 0);
    ASSERT_EQ(dq(5), 0);
    ASSERT_EQ(ddq(0), 0);
    ASSERT_EQ(ddq(1), 0);
    ASSERT_EQ(ddq(2), 0);
    ASSERT_EQ(ddq(3), 0);
    ASSERT_EQ(ddq(4), 0);
    ASSERT_EQ(ddq(5), 0);
    std::cout<<"[----------] Finsih: multi-point trajectory intial pose "<<std::endl;

    // check final condition
    time_step = tf;
    trajectory_generator.doFifthDegreeTrajectory(time_step);
    trajectory_generator.getTrajectory(q, dq, ddq);
    std::cout<<"[----------] Begin: multi-point trajectory final pose "<<std::endl;
    ASSERT_DOUBLE_EQ(q(0), qf(0));
    ASSERT_DOUBLE_EQ(q(1), qf(1));
    ASSERT_DOUBLE_EQ(q(2), qf(2));
    ASSERT_DOUBLE_EQ(q(3), qf(3));
    ASSERT_DOUBLE_EQ(q(4), qf(4));
    ASSERT_DOUBLE_EQ(q(5), qf(5));
    ASSERT_NEAR(dq(0),  0, 1*pow(10,-8));
    ASSERT_NEAR(dq(1),  0, 1*pow(10,-8));
    ASSERT_NEAR(dq(2),  0, 1*pow(10,-8));
    ASSERT_NEAR(dq(3),  0, 1*pow(10,-8));
    ASSERT_NEAR(dq(4),  0, 1*pow(10,-8));
    ASSERT_NEAR(dq(5),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(0),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(1),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(2),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(3),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(4),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(5),  0, 1*pow(10,-8));
    /*
    ASSERT_DOUBLE_EQ(dq(0), 0);
    ASSERT_DOUBLE_EQ(dq(1), 0);
    ASSERT_DOUBLE_EQ(dq(2), 0);
    ASSERT_DOUBLE_EQ(dq(3), 0);
    ASSERT_DOUBLE_EQ(dq(4), 0);
    ASSERT_DOUBLE_EQ(dq(5), 0);
    ASSERT_DOUBLE_EQ(ddq(0), 0);
    ASSERT_DOUBLE_EQ(ddq(1), 0);
    ASSERT_DOUBLE_EQ(ddq(2), 0);
    ASSERT_DOUBLE_EQ(ddq(3), 0);
    ASSERT_DOUBLE_EQ(ddq(4), 0);
    ASSERT_DOUBLE_EQ(ddq(5), 0);
    */
    std::cout<<"[----------] Finsih: multi-point trajectory final pose "<<std::endl;
    // check one point
    time_step = 0.5* tf;
    trajectory_generator.doFifthDegreeTrajectory(time_step);
    trajectory_generator.getTrajectory(q, dq, ddq);  
    std::cout<<"[----------] Begin: multi-point trajectory middle point test "<<std::endl;
    
    ASSERT_NEAR(q(0),  10.0500, 1*pow(10,-8));
    ASSERT_NEAR(q(1),  0, 1*pow(10,-8));
    ASSERT_NEAR(q(2),  -0.9990, 1*pow(10,-8));
    ASSERT_NEAR(q(3),  1.2750, 1*pow(10,-8));
    ASSERT_NEAR(q(4),  0.8000 , 1*pow(10,-8));
    ASSERT_NEAR(q(5),  -4.0000, 1*pow(10,-8));  


    ASSERT_DOUBLE_EQ(dq(0), 15*(qf(0)-q0(0))/(8*tf));
    ASSERT_DOUBLE_EQ(dq(1), 15*(qf(1)-q0(1))/(8*tf));
    ASSERT_DOUBLE_EQ(dq(2), 15*(qf(2)-q0(2))/(8*tf));
    ASSERT_DOUBLE_EQ(dq(3), 15*(qf(3)-q0(3))/(8*tf));
    ASSERT_DOUBLE_EQ(dq(4), 15*(qf(4)-q0(4))/(8*tf));
    ASSERT_DOUBLE_EQ(dq(5), 15*(qf(5)-q0(5))/(8*tf));

    ASSERT_NEAR(ddq(0),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(1),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(2),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(3),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(4),  0, 1*pow(10,-8));
    ASSERT_NEAR(ddq(5),  0, 1*pow(10,-8));    
     std::cout<<"[----------] Finish: multi-point trajectory middle point test "<<std::endl;  

    
 
    std::cout<<"[----------] Begin: multi-point trajectory one waypoint test "<<std::endl;
    time_step = 0.224* tf;
    trajectory_generator.doFifthDegreeTrajectory(time_step);
    trajectory_generator.getTrajectory(q, dq, ddq); 
    ASSERT_DOUBLE_EQ(q(0), 1.6524680806957059431);
    ASSERT_DOUBLE_EQ(q(1), -8.4397305721651196819);
    ASSERT_DOUBLE_EQ(q(2), -0.15418296972627149044);
    ASSERT_DOUBLE_EQ(q(3),  0.66311953351802888079);
    ASSERT_DOUBLE_EQ(q(4), 0.96879461144330236699);
    ASSERT_DOUBLE_EQ(q(5), 1.063838343299071898);

    ASSERT_NEAR(dq(0),  1.8038225436672, 1*pow(10,-8));
    ASSERT_NEAR(dq(1),  1.81288697856, 1*pow(10,-8));
    ASSERT_NEAR(dq(2),  -0.1814699865, 1*pow(10,-8));
    ASSERT_NEAR(dq(3),  0.1314343059456, 1*pow(10,-8));
    ASSERT_NEAR(dq(4),  -0.0362577395712, 1*pow(10,-8));
    ASSERT_NEAR(dq(5),  -1.087732187136, 1*pow(10,-8));

    ASSERT_NEAR(ddq(0),  1.14565312512, 1*pow(10,-8));
    ASSERT_NEAR(ddq(1),  1.151410176, 1*pow(10,-8));
    ASSERT_NEAR(ddq(2),  -0.1152561586176, 1*pow(10,-8));
    ASSERT_NEAR(ddq(3),  0.08347723776, 1*pow(10,-8));
    ASSERT_NEAR(ddq(4),  -0.02302820352, 1*pow(10,-8));
    ASSERT_NEAR(ddq(5),  -0.6908461056, 1*pow(10,-8));

    
    std::cout<<"[----------] Finish: multi-point trajectory one waypoint test "<<std::endl;
   
}


int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}
