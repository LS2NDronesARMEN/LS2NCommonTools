#ifndef PROPORTIONAL_DERIVATIVE_CONTROL
#define PROPORTIONAL_DERIVATIVE_CONTROL

#include "controllers/ControlLawBase.h"

namespace Controllers
{
  class PDControl : public ControlLawBase
  {
    public:
      /// Constructor
      PDControl(unsigned int _size);

      /// destructor
      ~PDControl();

      void Calculate();

      /// Set the State value
      void SetState(double _state, double _d_state);
      void SetState(Eigen::MatrixXd _state, Eigen::MatrixXd _d_state);

      /// Set the reference value
      void SetRef(double _ref, double _d_ref);
      void SetRef(Eigen::MatrixXd _ref, Eigen::MatrixXd _d_ref);

      /// Set the Gain vaules
      void SetGains(std::string type, double _k);
      void SetGains(std::string type, Eigen::MatrixXd _k);

    protected:
      /// Vector of State values
      Eigen::VectorXd state, d_state;

      /// Vector of desired state values
      Eigen::VectorXd ref, d_ref;

      /// Vector of gains
      Eigen::MatrixXd kp, kd;
  };
}
#endif
