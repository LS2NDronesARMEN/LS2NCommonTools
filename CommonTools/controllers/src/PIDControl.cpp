#include "controllers/PIDControl.h"

namespace Controllers
{
  /** Constructor
  *
  **/
  PIDControl::PIDControl(unsigned int _size)
  : PDControl(_size)
  {
    if(size > 0)
    {
      ki = 0.1*Eigen::MatrixXd::Identity(size,size);
      integral = Eigen::VectorXd(size).setZero();
      integral_sat = Eigen::VectorXd(size).setOnes();
    }
    else
    {
      ROS_ERROR("must give a size to the control block");
    }
    return;
  }


  /** Destructor
  *
  **/
  PIDControl::~PIDControl()
  {
    return;
  }


  /** Calculate
  *
  **/
  void PIDControl::Calculate()
  {
    if(state.rows()>0 && ref.rows()>0)
    {
      // if the first iteration, do not integrate
      if(first_iter == true)
      {
        dt = 0;
        t_last = ros::Time::now().toSec();
        integral.setZero();
        first_iter = false;
      }
      else
      {
        //calculate integral
        dt = ros::Time::now().toSec() - t_last;
        t_last = ros::Time::now().toSec();
        integral = (ki*(ref-state)*dt) + integral;
        // if there is a long gap, reset integrator
        if(dt > 0.1)
        {
          std::cout<< "reseting integral after time gap dt = "<<dt <<std::endl;
          dt = 0;
          first_iter = true;
          integral.setZero();
        }
        // saturate integrator
        for(int i=0; i<integral.rows(); i++)
        {
          if(integral(i) > integral_sat(i))
          {
            // positive saturation
            integral(i) = integral_sat(i);
            // warn
            ROS_WARN_THROTTLE(1,"Integration is saturating in PID control at: %f",integral(i));
          }
          else if(integral(i) < -integral_sat(i))
          {
            // negative saturation
            integral(i) = -integral_sat(i);
            // warn
            ROS_WARN_THROTTLE(1,"Integration is saturating in PID control at: %f",integral(i));
          }
        }
      }
      // do Control Law
      out = (kp * (ref-state)) + (kd * (d_ref-d_state)) + integral;

    }
    else
    {
      ROS_ERROR("Doing control on an incomplete expression");
    }
    return;
  }


  /** void PIDControl::SetGains(double _state)
  *
  **/
  void PIDControl::SetGains(std::string type, double _k)
  {
    if(type == "I")
    {
      ki = _k*Eigen::MatrixXd::Identity(size,size);;
    }
    else
    {
      PDControl::SetGains(type, _k);
    }
    return;
  }
  /** \overload void PIDControl::SetGains(Eigen::MatrixXd _state);
  *
  **/
  void PIDControl::SetGains(std::string type, Eigen::MatrixXd _k)
  {
    if(_k.rows() == size && _k.cols() ==size)
    {
      if(type == "I")
      {
        ki = _k;
      }
      else
      {
        PDControl::SetGains(type, _k);
      }
    }
    else if(_k.rows() == size)
    {
      if(type == "I")
      {
        ki = _k.asDiagonal();
      }
      else
      {
        PDControl::SetGains(type, _k);
      }
    }
    else
    {
      ROS_ERROR("Invalid input size");
    }
    return;
  }


  /**
  *
  **/
  void PIDControl::SetISat(double _k)
  {
    integral_sat = _k * Eigen::VectorXd(size).setOnes();
    return;
  }
  /**
  *
  **/
  void PIDControl::SetISat(Eigen::MatrixXd _k)
  {
    if(_k.rows() == size)
    {
      integral_sat = _k.block(0,0,_k.rows(),1);
    }
    else if(_k.rows() == size)
    {
      integral_sat = _k;
    }
    else
    {
      ROS_ERROR("Invalid integral saturation input size");
    }
    return;
  }



}
