cmake_minimum_required(VERSION 2.8.3)
project(controllers)
add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
	roscpp
)

find_package(Eigen3 REQUIRED)

catkin_package(
	LIBRARIES controllers_LIB
	INCLUDE_DIRS include
  DEPENDS EIGEN3
)

include_directories(
	LIBRARIES controllers_LIB
	INCLUDE_DIRS include
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIRS}
)

ADD_LIBRARY(controllers_LIB
	src/ControlLawBase.cpp
	src/PControl.cpp
	src/PDControl.cpp
	src/PIDControl.cpp
)

target_link_libraries(
	controllers_LIB
	${catkin_LIBRARIES}
)

add_executable(test_node src/controller_block_test.cpp)

target_link_libraries(test_node
	${catkin_LIBRARIES}
	controllers_LIB
)
