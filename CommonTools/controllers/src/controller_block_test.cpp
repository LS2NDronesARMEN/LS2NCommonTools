#include <iostream>
#include "controllers/PControl.h"
#include "controllers/PDControl.h"
#include "controllers/PIDControl.h"

#define RATE 20

//using namespace std;


int main(int argc, char **argv)
{
  // start ROS
  ros::init(argc, argv, "controller_test");
  ros::NodeHandle nh;
  ros::Rate loopRate(RATE);

  Eigen::VectorXd state(3);
  Eigen::Vector3d ref(3);
  Eigen::VectorXd d_state(3);
  Eigen::Vector3d d_ref(3);

  Eigen::Vector3d Pgains;
  Eigen::MatrixXd Dgains(3,1);

  state << -1, -2, -3;
  ref << 1, 2, 3;
  d_state << 1, 2, 3;
  d_ref << 1, 2, 3;

  Pgains << 1, 1, 1;
  Dgains << 5, 5, 5;

  //Controllers::PControl Pcon(state.size());
  Controllers::PIDControl PDcon(state.size());

  ROS_INFO("Controller Built");

  while(ros::ok())
  {
    PDcon.SetState(state,d_state);
    PDcon.SetRef(ref,d_ref);
    PDcon.SetGains("P",Pgains);
    PDcon.SetGains("D",Dgains);

    PDcon.Calculate();
    std::cout <<"Control Output: \n"<< PDcon.getResult() << std::endl;
    loopRate.sleep(); // sleep
  }
} // end main
