#include "librobot_traj_generate/librobot_traj_generate.hpp"


LibTrajGenerate::LibTrajGenerate(const Eigen::VectorXd q0, const Eigen::VectorXd qf, double tf):q0_(q0),qf_(qf), travelling_time_(tf)
{

        distance_ = qf_- q0_;

}

LibTrajGenerate::~LibTrajGenerate()
{
}



void LibTrajGenerate::doFifthDegreeTrajectory(double time_step)
{

        double factor = time_step/travelling_time_; 

        if (factor<= 1)
        {
               r_ = 10 * pow(factor, 3) -15 * pow(factor, 4) + 6 * pow(factor, 5);

               dr_ = 30 * pow(time_step, 2)/pow(travelling_time_,3)  - 60 * pow(time_step, 3)/pow(travelling_time_,4) + 30 * pow(time_step, 4)/pow(travelling_time_,5);

               ddr_ = 60 * time_step/pow(travelling_time_, 3) - 180 * pow(time_step, 2)/pow(travelling_time_, 4) + 120 * pow(time_step, 3)/pow(travelling_time_,5);
        }
        else
        {
                std::cout<<"The time step is reaching the travelling time"<<std::endl;
        }
        
        

        


}



void LibTrajGenerate::getTrajectory(Eigen::VectorXd &q, Eigen::VectorXd &dq, Eigen::VectorXd &ddq)
{

    q = q0_ + r_ * distance_;

    dq = dr_* distance_;

    ddq = ddr_ * distance_;

}