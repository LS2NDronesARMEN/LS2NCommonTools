#ifndef CONTROLLER_BASE
#define CONTROLLER_BASE

#include <ros/ros.h>
#include <Eigen/Eigen>
#include <string>

namespace Controllers
{
  class ControlLawBase
  {
    public:
      /// Constructor
      ControlLawBase(unsigned int _size);

      /// destructor
      virtual ~ControlLawBase();

      /// do control calculations
      virtual void Calculate() = 0;

      /// return control output
      inline Eigen::VectorXd getResult(){if(first_iter){Calculate();}return out;}

    protected:

      /// Output Vector
      Eigen::VectorXd out;

      /// size of Output
      unsigned int size;

      /// First calculation flag
      bool first_iter = true;

      /// Conversion functions
      void convert2VXd(Eigen::VectorXd &_value, double _input);
      void convert2VXd(Eigen::VectorXd &_value, Eigen::VectorXd _input);
      void convert2VXd(Eigen::VectorXd &_value, Eigen::MatrixXd _input);

    private:
      /// NodeHandle
      ros::NodeHandle nh;
  };
}
#endif
