#include "controllers/ControlLawBase.h"

namespace Controllers
{
  /** Construstor
    *
  **/
  ControlLawBase::ControlLawBase(unsigned int _size)
  : size(_size)
  {
    // chck that size is positive definite integer
    if(size <= 0)
    {
      ROS_WARN("you should give a positive state size" );
      size = 0;
    }
    // chreate  node handle
    ros::NodeHandle n("~");
    nh = n;
    return;
  }

  /** Destructor
    *
  **/
  ControlLawBase::~ControlLawBase()
  {
    nh.~NodeHandle();
    return;
  }


  /**  void ControlLawBase::convert2VXd(Eigen::VectorXd &_value, double _input)
    *
  **/
  void ControlLawBase::convert2VXd(Eigen::VectorXd &_out, double _in)
  {
    if(_out.rows() == 1)
    {
      // copy first row of matrix to state vector
      _out(0) = _in;
    }
    else
    {
      ROS_ERROR("Cannot give a state vector of size 1 to one of size %d", int(_out.rows()) );
    }
    return;
  }


  /**  void ControlLawBase::convert2VXd(Eigen::VectorXd &_value, Eigen::VectorXd _input)
    *
  **/
  void ControlLawBase::convert2VXd(Eigen::VectorXd &_out, Eigen::VectorXd _in)
  {
    if(_out.rows() == _in.rows())
    {
      _out = _in;
    }
    else
    {
      ROS_ERROR("Cannot give a state vector of size %d to one of size %d", int(_in.rows()), int(_out.rows()) );
    }
    return;
  }


  /**  void ControlLawBase::convert2VXd(Eigen::VectorXd &_value, Eigen::MatrixXd _input)
    *
  **/
  void ControlLawBase::convert2VXd(Eigen::VectorXd &_out, Eigen::MatrixXd _in)
  {
    if(_out.rows() == _in.rows())
    {
      // copy first row of matrix to state vector
      _out = _in.block(0,0,_in.rows(),1);
    }
    else
    {
      ROS_ERROR("Cannot give a state vector of size %d to one of size %d", int(_in.rows()), int(_out.rows()) );
    }
    return;
  }







}
