#ifndef PROPORTIONAL_CONTROL
#define PROPORTIONAL_CONTROL

#include "controllers/ControlLawBase.h"

namespace Controllers
{
  class PControl : public ControlLawBase
  {
    public:
      /// Constructor
      PControl(unsigned int _size);

      /// destructor
      ~PControl();

      void Calculate();

      /// Set the State value
      void SetState(double _state);
      void SetState(Eigen::MatrixXd _state);

      /// Set the reference value
      void SetRef(double _ref);
      void SetRef(Eigen::MatrixXd _ref);

      /// Set the Gain vaules
      void SetGains(double _Kp);
      void SetGains(Eigen::MatrixXd _Kp);

    protected:
      /// Vector of State values
      Eigen::VectorXd state;

      /// Vector of desired state values
      Eigen::VectorXd ref;

      /// Vector of gains
      Eigen::MatrixXd kp;

  };
}
#endif
