#ifndef PID_CONTROL
#define PID_CONTROL

#include "controllers/PDControl.h"

namespace Controllers
{
  class PIDControl : public PDControl
  {
    public:
      /// Constructor
      PIDControl(unsigned int _size);

      /// destructor
      ~PIDControl();

      void Calculate();

      /// Set the Gain vaules
      void SetGains(std::string type, double _k);
      void SetGains(std::string type, Eigen::MatrixXd _k);

      /// Set the Integration Limit values
      void SetISat( double _k);
      void SetISat( Eigen::MatrixXd _k);

    protected:
      /// Vector of State values
      Eigen::VectorXd integral, integral_sat;

      /// Integration gain
      Eigen::MatrixXd ki;

      /// Time step for calculating integration
      double t_last, dt;

  };
}
#endif
