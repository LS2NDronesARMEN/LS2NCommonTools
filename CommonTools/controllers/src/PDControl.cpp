#include "controllers/PDControl.h"

namespace Controllers
{
  /** Constructor
  *
  **/
  PDControl::PDControl(unsigned int _size)
  : ControlLawBase(_size)
  {
    if(size > 0)
    {
      kp = Eigen::MatrixXd::Identity(size,size);
      kd = 0.5*Eigen::MatrixXd::Identity(size,size);
      state = Eigen::VectorXd(size);
      ref = Eigen::VectorXd(size);
      d_state = Eigen::VectorXd(size);
      d_ref = Eigen::VectorXd(size);
      out = Eigen::VectorXd(size);
    }
    else
    {
      ROS_ERROR("must give a size to the control block");
    }
    return;
  }


  /** Destructor
  *
  **/
  PDControl::~PDControl()
  {
    return;
  }


  /** Calculate
  *
  **/
  void PDControl::Calculate()
  {
    out = (kp * (ref-state)) + (kd * (d_ref-d_state));
    first_iter = false;
    return;
  }



  /** void PDControl::SetState(double _state)
  *
  **/
  void PDControl::SetState(double _state, double _d_state)
  {
    convert2VXd(state,_state);
    convert2VXd(d_state,_d_state);
    return;
  }
  /** \overload void PDControl::SetState(Eigen::MatrixXd _state);
  *
  **/
  void PDControl::SetState(Eigen::MatrixXd _state, Eigen::MatrixXd _d_state)
  {
    convert2VXd(state,_state);
    convert2VXd(d_state,_d_state);
    return;
  }



  /** void PDControl::SetRef(double _state)
  *
  **/
  void PDControl::SetRef(double _ref, double _d_ref)
  {
    convert2VXd(ref,_ref);
    convert2VXd(d_ref,_d_ref);
    return;
  }
  /** \overload void PDControl::SetRef(Eigen::MatrixXd _state);
  *
  **/
  void PDControl::SetRef(Eigen::MatrixXd _ref, Eigen::MatrixXd _d_ref)
  {
    convert2VXd(ref,_ref);
    convert2VXd(d_ref,_d_ref);
    return;
  }




  /** void PDControl::SetGains(double _state)
  *
  **/
  void PDControl::SetGains(std::string type, double _k)
  {
    if(type == "P")
    {
      kp = _k*Eigen::MatrixXd::Identity(size,size);
    }
    else if(type == "D")
    {
      kd = _k*Eigen::MatrixXd::Identity(size,size);
    }

    return;
  }
  /** \overload void PDControl::SetGains(Eigen::MatrixXd _state);
  *
  **/
  void PDControl::SetGains(std::string type, Eigen::MatrixXd _k)
  {
    if(_k.rows() == size && _k.cols() ==size)
    {
      if(type == "P")
      {
        kp = _k.diagonal().asDiagonal();
      }
      else if(type == "D")
      {
        kd = _k.diagonal().asDiagonal();
      }
    }
    else if(_k.rows() == size)
    {
      if(type == "P")
      {
        kp = _k.asDiagonal();
      }
      else if(type == "D")
      {
        kd = _k.asDiagonal();
      }
    }
    else
    {
      ROS_ERROR("Cannot Set the gains to that size");
    }
    return;
  }


}
